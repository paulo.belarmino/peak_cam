########## IMPORTS ##########

import pygal as pg
import cv2
import numpy as np

from scipy.signal import find_peaks

########## FUNCTION DEFINITIONS ##########

class TrackDetection():

    def display(self, img, win_title="Output"):
        cv2.namedWindow(win_title, cv2.WINDOW_NORMAL)
        cv2.imshow(win_title, img)
        cv2.waitKey(5)

    def display4(self, imgs, win_title="Output", dims=(2500,2500)):
        cv2.namedWindow(win_title, cv2.WINDOW_NORMAL)   # Create window with freedom of dimensions
        hor1 = np.concatenate((cv2.resize(imgs[0], dims), cv2.resize(imgs[1], dims)), axis=1)   # Concatenate 0-1 horizontally
        hor2 = np.concatenate((cv2.resize(imgs[2], dims), cv2.resize(imgs[3], dims)), axis=1)   # Concatenate 2-3 horizontally
        all4 = np.concatenate((hor1, hor2), axis=0)     # Concatenate all 4
        cv2.imshow(win_title, all4)
        cv2.waitKey(5)

    def putText(self, img, str, org, color):
        cv2.putText(img, str, org=org, 
                    fontFace=cv2.FONT_HERSHEY_SIMPLEX, 
                    fontScale=1, color=color,
                    thickness=3, lineType=1)

    def skeletonize(self, img):
        """ OpenCV function to return a skeletonized version of img, a Mat object"""

        #  hat tip to http://felix.abecassis.me/2011/09/opencv-morphological-skeleton/

        img = img.copy() # don't clobber original
        skel = img.copy()

        skel[:,:] = 0
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5,5))

        while True:
            eroded = cv2.morphologyEx(img, cv2.MORPH_ERODE, kernel)
            temp = cv2.morphologyEx(eroded, cv2.MORPH_DILATE, kernel)
            temp  = cv2.subtract(img, temp)
            skel = cv2.bitwise_or(skel, temp)
            img[:,:] = eroded[:,:]
            if cv2.countNonZero(img) == 0:
                break

        return skel

    def preprocessing1520p(self, img, region=0, plot=False):
        # 0 = tracks, 1 = plate_left, 2 = plate_right, 3 = BEV
        if region == 0:
            gaussian_ker = (35,35)      # Gaussian blur kernel
            thresh_neighb_size = 9      # Adaptive threshold neighborhood size
            thresh_const = 2.5          # Adaptive threshold constant value
            open_ker = (6,6)            # Opening kernel
        # elif region == 1:
        #     gaussian_ker = (3,3)
        #     thresh_neighb_size = 31
        #     thresh_const = 6
        #     open_ker = (9,9)
        #     canny_min = 20
        #     canny_max = 50
        # elif region == 2:
        #     gaussian_ker = (3,3)
        #     thresh_neighb_size = 31
        #     thresh_const = 2
        #     open_ker = (6,6)
        #     canny_min = 40
        #     canny_max = 100
        else: # region == 3:
            gaussian_ker = (21,21)
            thresh_neighb_size = 9
            thresh_const = 2
            open_ker = (4,50)         # vertical structuring element

        # Convert to grayscale, apply Gaussian blur, adaptive threshold,
        # opening to denoise, Canny edge detector
        gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)    
        blurred = cv2.GaussianBlur(gray, gaussian_ker, 0)

        thresh = cv2.adaptiveThreshold(blurred, 255,
            cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY_INV, thresh_neighb_size, thresh_const)

        # resized_image = cv2.resize(thresh, (600, 600))
        # cv2.imshow("thresh", resized_image)
        # cv2.waitKey(5)
        
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, open_ker)
        opening = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel)

        if region == 0:
            # if plot:
            #     self.display4([gray, blurred, thresh, opening])
            return opening

        elif region == 3:
            kernel2 = cv2.getStructuringElement(cv2.MORPH_RECT, (1,1000))
            dilate = cv2.morphologyEx(opening, cv2.MORPH_DILATE, kernel2)
            resized_image = cv2.resize(opening, (600, 600))
            cv2.imshow("opening_region3", resized_image)
            cv2.waitKey(5)
            # kernel5 = cv2.getStructuringElement(cv2.MORPH_RECT, (2,1600))     # Test
            # erode = cv2.morphologyEx(dilate, cv2.MORPH_ERODE, kernel5)

            # if plot:
            #     self.display4([blurred, thresh, opening, dilate])
            return dilate
        # edges = cv2.Canny(opening, canny_min, canny_max)

        # if plot:
        #     display4([blurred, thresh, opening, edges])

        # return edges

    def hough1520p(self, img, canny, region=0, plot=False):
        # 0 = tracks, 1 = plate_left, 2 = plate_right
        if region == 0:
            thresh = 190        # 210  Minimum number of votes for candidate line
            minLength = 250     # 300 Minimum line length
            maxGap = 30         # 40 Maximum gap between segments that belong to the same line
            lineColor = (255, 0, 0)
        # elif region == 1:
        #     thresh = 80
        #     minLength = 180
        #     maxGap = 40
        #     lineColor = (255, 255, 0)
        # elif region == 2:
        #     thresh = 80
        #     minLength = 150
        #     maxGap = 20
        #     lineColor = (255, 255, 0)

        # Given a preprocessed image with canny edges,
        # Detects points that form a line using Probabilistic Hough Transform
        lines = cv2.HoughLinesP(canny, 1, np.pi/180, threshold=thresh, minLineLength=minLength, maxLineGap=maxGap)

        # print(lines)

        # Draw lines on the image
        try:
            if plot:
                copy = img.copy()
                for line in lines:
                    x1, y1, x2, y2 = line[0]
                    cv2.line(copy, (x1, y1), (x2, y2), lineColor, 4)
                    if x2-1 != 0:
                        slope = (y2-y1)/(x2-x1)
                    else:
                        slope = 999
                    # cv2.putText(copy,f'{round(slope,2)}', org=(x2,y2+20), 
                    #                         fontFace=cv2.FONT_HERSHEY_SIMPLEX, 
                    #                         fontScale=2,
                    #                         color=(0,0,0),
                    #                         thickness=5,
                    #                         lineType=1)
                #self.display(copy)
        except:
            pass

        return lines, copy

    def getYIntercept(self, lines, slope_mean):
        x_values, y_values = np.concatenate([lines[:,0], lines[:,2]]), np.concatenate([lines[:,1], lines[:,3]])
        y_intercept_mean = (y_values - (slope_mean * x_values)).mean()
        return y_intercept_mean

    def getExtremePoints(self, img, slope, intercept):

        # Max y value
        img_bottom = img.shape[0]

        # x points
        if np.abs(slope) < 0.001:
            max_x, min_x = 999, 999
        else:
            max_x = (img_bottom - intercept) / slope
            min_x = (0 - intercept) / slope

        # extreme points
        p1 = (int(min_x), int(0))
        p2 = (int(max_x), int(img_bottom))

        return p1, p2

    def seg_intersect(self, p1, p2, p3, p4) :
        # Point p = (x,y)
        # Calculating the slopes of the two lines
        slope1 = (p2[1] - p1[1]) / (p2[0] - p1[0])
        slope2 = (p4[1] - p3[1]) / (p4[0] - p3[0])

        # Checking if the lines are parallel (slopes are equal)
        if slope1 == slope2:
            return None  # Lines are parallel, no intersection point

        # Calculating the x-coordinate of the intersection point
        x_intersection = (slope1 * p1[0] - slope2 * p3[0] + p3[1] - p1[1]) / (slope1 - slope2)

        # Calculating the y-coordinate of the intersection point
        y_intersection = slope1 * (x_intersection - p1[0]) + p1[1]

        # Creating and returning the intersection point
        return (int(x_intersection), int(y_intersection))

    def BEV(self, img, lines, plot=False):

        img_dims = img.shape
        # print(img_dims)

        # if lines is None or len(lines) == 0:
        #     print("Lines_tracks is empty or None")
        #     return None, None, None, None

        # print(lines)
        
        # Calculate slopes
        # print(lines.shape)
        lines = np.squeeze(lines)
        # print(lines)

        # if lines.ndim != 2 or lines.shape[1] < 4:
        #     print(f"Lines is not properly shaped: {lines.shape}")
        #     return None, None, None, None
        
        # Denominator: lines[:,2] - lines[:,0]
        denominator = lines[:,2] - lines[:,0]

        # Handle cases where denominator is zero
        # To avoid division by zero, replace zero with a very small number or handle it in another way
        small_number = 1e-10  # A small number to prevent division by zero
        denominator_safe = np.where(denominator == 0, small_number, denominator)

        # Calculate slopes
        slopes = (lines[:,3] - lines[:,1]) / denominator_safe

        # Filter out horizontal lines
        horizontal_thresh = 0.15
        lines_f = lines[np.abs(slopes) > horizontal_thresh]
        slopes_f = slopes[np.abs(slopes) > horizontal_thresh]

        # Replace inf-values
        slopes_f[(slopes_f == np.inf) | (slopes_f == -np.inf)] = 99

        ###########################################################
        #### Case 1: all slopes are positive \\ or negative // ####
        ###########################################################
        if min(slopes_f) >= 0 or max(slopes_f) <=0:
            # Left-most slope > right-most slope
            ind1, ind2 = np.where(slopes_f == max(slopes_f))[0], np.where(slopes_f == min(slopes_f))[0]
            slope1, slope2 = slopes_f[ind1], slopes_f[ind2]
            line1, line2 = lines_f[ind1][0], lines_f[ind2][0] # (x1,y1,x2,y2)

        ################################################################
        #### Case 2: some slopes are positive, some are negative /\ ####
        ################################################################
        else:
            # Left-most slope smallest among negatives, right-most slope smallest among positives
            slopes_neg, slopes_pos = slopes_f[slopes_f < 0], slopes_f[slopes_f > 0]
            lines_neg, lines_pos = lines_f[slopes_f < 0], lines_f[slopes_f > 0]
            ind1, ind2 = np.where(slopes_neg == max(slopes_neg))[0], np.where(slopes_pos == min(slopes_pos))[0]
            slope1, slope2 = slopes_neg[ind1], slopes_pos[ind2]
            line1, line2 = lines_neg[ind1][0], lines_pos[ind2][0] # (x1,y1,x2,y2)

        intercept1 = line1[1] - (slope1 * line1[0])
        intercept2 = line2[1] - (slope2 * line2[0])

        # RoI
        img_floor = int(img_dims[0]*0.6)
        x11, x21 = (img_floor - intercept1) / slope1, (0 - intercept1) / slope1
        x12, x22 = (img_floor - intercept2) / slope2, (0 - intercept2) / slope2
        p2, p1 = (int(x11[0]), int(img_floor)), (int(x21[0]), int(0))
        q2, q1 = (int(x12[0]), int(img_floor)), (int(x22[0]), int(0))

        # Display RoI
        if plot:
            copy = img.copy()
            cv2.line(copy, p1, p2, [0, 255, 255], 4)
            cv2.line(copy, q1, q2, [0, 0, 255], 4)
            self.display(copy)

        # Source / destination points
        # top-left, bottom-left, top-right, bottom-right
        srcPts = np.array([p1, p2, q1, q2], dtype=np.float32)
        (b, r, _) = img.shape
        thresh = 350
        dstPts = np.array([[thresh, 0], [thresh, b], [r-thresh, 0], [r-thresh, b]], dtype=np.float32)

        # Compute the matrix that transforms the perspective
        transformMatrix = cv2.getPerspectiveTransform(srcPts, dstPts)

        # Compute the inverse matrix for reversing the perspective transform
        inverseTransformMatrix = cv2.getPerspectiveTransform(dstPts, srcPts)

        # Warp the perspective
        # image.shape[:2] takes the height, width 
        # [::-1] inverses it to width, height
        warpedImage = cv2.warpPerspective(img, transformMatrix, img.shape[:2][::-1], flags=cv2.INTER_LINEAR)
        
        return warpedImage, srcPts, transformMatrix, inverseTransformMatrix

    def evaluateRatios(self, x, ref1=0.98, ref2=3.6, plot=False):
        # Identify rail groove based on width ratios (see GrooveRailProfiles.xlsx)
        # c/b = 0.98
        # c/(a-b-c) = 3.60
        # Checks ratios first from left to right, then from right to left

        # Institute's rails have a repeating pattern => discard last peaks from the right
        if len(x) > 7: # minimum length for repeating pattern = 8 (2 * 4 groove peaks)
            x = x[:5]

        l = len(x)
        ratioL2R, errorL2R = np.zeros(l), np.full(l, 9.99)
        ratioR2L, errorR2L = np.zeros(l), np.full(l, 9.99)

        # Left to Right
        for i in range(1,l-1):
            r = (x[i+1]-x[i])/(x[i]-x[i-1])
            ratioL2R[i] = r
            errorL2R[i] = np.sqrt((1 + np.abs(ref1-r))**2) - 1

        # Right to Left
        for i in range(l-2, 0, -1):
            r = (x[i]-x[i-1])/(x[i+1]-x[i])
            ratioR2L[i] = r
            errorR2L[i] = np.sqrt((1 + np.abs(ref2-r))**2) - 1

        if plot:
            print('LEFT TO RIGHT')
            print(f'Ratios: {np.round(ratioL2R,2)}')
            print(f'Error:  {np.round(errorL2R,2)}')
            print('RIGHT TO LEFT')
            print(f'Ratios: {np.round(ratioR2L,2)}')
            print(f'Error:  {np.round(errorR2L,2)}')
            print()

        errfinal = errorL2R[1:l-1] + errorR2L[2:]       # Sums errors from both sweeps (R2L dislocated by 1 to the right)

        if errfinal.size == 0:  # Check if errfinal is empty
            return [], 9.99  # or return any default values
    

        indsmallest = np.argmin(errfinal)               # Takes index of smallest total error

        # Pick out 2 x-values that define groove edges
        # +1 to compensate displacement in error index
        return x[indsmallest+1:indsmallest+3], np.min(errfinal)

    def matchPeaks(self, x, y):
        # Apply value-matching on peak arrays
        # Ex.:
        # x = [2, 50, 98, 156]
        # y = [0, 45, 70, 105, 125, 135, 160, 190]
        # match = [0, 45, 105, 160]

        # len(arr2) > len(arr1)
        if len(x) < len(y):
            arr1, arr2 = x, y
            order = 1
        else:
            arr1, arr2 = y, x
            order = 0

        dist_matrix = np.abs(arr2[:,None] - arr1)       # Distance matrix from each element from arr2 to each element from arr1
        mask = np.full(len(arr2), False, dtype=bool)    # Mask to select smallest distance values

        for i in range(len(arr1)):                      
            ind = np.argmin(dist_matrix[:,i])
            mask[ind] = True

        # print(dist_matrix, '\n')
        # print(mask)

        if order:
            return arr1, arr2[mask]
        else:
            return arr2[mask], arr1

    def removeDoublePeaks(self, arr1, arr2):
        if len(arr1) != len(arr2):
            print('removeDoublePeaks: arrays have different lengths!')
            return arr1, arr2
        # given two matched peak arrays of same length
        arr1, arr2 = np.array(arr1), np.array(arr2)
        thresh = 35  # maximum separation between two peak coordinates to consider them a single peak
        diff1, diff2 = np.diff(arr1), np.diff(arr2)
        ind = np.ones(len(arr1), dtype=bool)
        for i in range(len(diff1)):
            if diff1[i] < thresh or diff2[i] < thresh:
                ind[i+1] = False

        return arr1[ind], arr2[ind]

    def pushOutliers(self, data):
        if len(data) == 0:
            return np.array([]), np.array([])  # Return empty arrays if data is empty

        # Reorder array by std dev in ascending order
        d = np.abs(data - np.median(data, axis=0))
        mdev = np.median(d)
        s = d / (mdev if mdev else 1.)
        ind = np.argsort(s)
        return data[ind], ind

    def detectGroove(self, img, baseImg, plot=False):
        plot = False
        # Preprocess without applying the Canny Edge detector
        # preprocessed = preprocessing760p(img, region=3, plot=plot)
        preprocessed = self.preprocessing1520p(img, region=3, plot=plot)

        print(np.sum(preprocessed))

        # Find peaks in the 1D histogram of each image quarter
        # Note: y values start at the top of the image
        quarter_height = int(img.shape[0]/4)
        sum1 = np.sum(preprocessed[:quarter_height,:], axis=0)
        sum2 = np.sum(preprocessed[quarter_height:2*quarter_height,:], axis=0)
        sum3 = np.sum(preprocessed[2*quarter_height:3*quarter_height,:], axis=0)
        sum4 = np.sum(preprocessed[3*quarter_height:,:], axis=0)

        # smoothen the curve
        sum1 = np.convolve(sum1, np.ones(7)/7, mode="same")
        sum2 = np.convolve(sum2, np.ones(7)/7, mode="same")
        sum3 = np.convolve(sum3, np.ones(7)/7, mode="same")
        sum4 = np.convolve(sum4, np.ones(7)/7, mode="same")

        # adaptive threshold
        height_thresh = max(max(sum1), max(sum2), max(sum3), max(sum4))*0.4
        peaks1, _ = find_peaks(sum1, height=height_thresh, distance=35)
        peaks2, _ = find_peaks(sum2, height=height_thresh, distance=35)
        peaks3, _ = find_peaks(sum3, height=height_thresh, distance=35)
        peaks4, _ = find_peaks(sum4, height=height_thresh, distance=35)

        if plot:
            print('')
            print(f'Peaks1: {peaks1}')
            print(f'Peaks2: {peaks2}')
            print(f'Peaks3: {peaks3}')
            print(f'Peaks4: {peaks4}')
            print()

        # Perform peak matching
        for i in range(3):
            peaks1, peaks2 = self.matchPeaks(peaks1, peaks2)
            peaks1, peaks3 = self.matchPeaks(peaks1, peaks3)
            peaks1, peaks4 = self.matchPeaks(peaks1, peaks4)
            peaks2, peaks3 = self.matchPeaks(peaks2, peaks3)
            peaks2, peaks4 = self.matchPeaks(peaks2, peaks4)
            peaks3, peaks4 = self.matchPeaks(peaks3, peaks4)

        if plot:
            print(f'Peaks1: {peaks1}')
            print(f'Peaks2: {peaks2}')
            print(f'Peaks3: {peaks3}')
            print(f'Peaks4: {peaks4}')
            print()

        # Perform peak removal
        peaks1, peaks2 = self.removeDoublePeaks(peaks1, peaks2)
        peaks3, peaks4 = self.removeDoublePeaks(peaks3, peaks4)
        peaks1, peaks4 = self.removeDoublePeaks(peaks1, peaks4)
        peaks2, peaks3 = self.removeDoublePeaks(peaks2, peaks3)

        if plot:
            print(f'Peaks1: {peaks1}')
            print(f'Peaks2: {peaks2}')
            print(f'Peaks3: {peaks3}')
            print(f'Peaks4: {peaks4}')
            print()

        # Plot detected peaks
        if plot:
            peaks_chart, peaks_lables = np.full(len(sum1), None), np.full(len(sum1), None)
            peaks_chart[peaks1] = sum1[peaks1]  # None, ..., None, y_peak, None,..., None
            peaks_lables[peaks1] = peaks1       # None, ..., None, x_peak, None,..., None

            chart = pg.Line(dots_size=0.2)
            chart.add('Threshold', np.full(len(sum1), height_thresh), show_dots=False)
            chart.add('Part 1', sum1)
            chart.add('Part 2', sum2)
            chart.add('Part 3', sum3)
            chart.add('Part 4', sum4)
            chart.add('Peaks', peaks_chart, stroke=False, dots_size=4)
            chart.x_labels = peaks_lables
            chart.render_in_browser()

        # Determine reference points of the rail groove
        groove1, e1 = self.evaluateRatios(peaks1, plot=plot)
        groove2, e2 = self.evaluateRatios(peaks2, plot=plot)
        groove3, e3 = self.evaluateRatios(peaks3, plot=plot)
        groove4, e4 = self.evaluateRatios(peaks4, plot=plot)

        g = np.array([groove1, groove2, groove3, groove4])
        h = np.array([quarter_height, 2*quarter_height, 3*quarter_height, 4*quarter_height])
        e = np.array([e1, e2, e3, e4])

        if plot:
            print(f'g: {g}')
            print(f'h: {h}')
            print(f'e: {e}')

        # print(g)

        # # If mean error is large, perform majority vote
        # if e.mean() > 2:
        #     self.putText(baseImg, '!', (10,40), (255,0,0))   # signal majority vote
        #     g0, ind = self.pushOutliers(g[:,0])
        #     g1, _ = self.pushOutliers(g[:,1])
        #     gsort = np.transpose(np.vstack([g0,g1]))
        #     hsort = h[ind]
        # else:
        #     einds = np.argsort(e)
        #     gsort = g[einds]        # sort grooves based on increasing error values
        #     hsort = h[einds]        # sort heights based on increasing error values         

        # return gsort, hsort, e

        # Check if 'g' has any non-empty arrays before accessing
        if np.all([len(gg) > 0 for gg in g]):
            if e.mean() > 2:
                self.putText(baseImg, '!', (10,40), (255,0,0))   # signal majority vote
                g0, ind = self.pushOutliers(g[:,0])
                g1, _ = self.pushOutliers(g[:,1])
                gsort = np.transpose(np.vstack([g0,g1]))
                hsort = h[ind]
            else:
                einds = np.argsort(e)
                gsort = g[einds]        # sort grooves based on increasing error values
                hsort = h[einds]        # sort heights based on increasing error values
        else:
            gsort, hsort = np.array([]), np.array([])
        
        return gsort, hsort, e


    def processGroove(self, baseImg, inverseTransform, grooveX, grooveY, grooveE):
        # Transform reference points back to original image
        # top-left, top-right, bottom-left, bottom-right
        x = np.concatenate([grooveX[0], grooveX[1]])
        x2 = np.concatenate([grooveX[2], grooveX[3]])
        y = np.array([grooveY[0], grooveY[0], grooveY[1], grooveY[1]])
        y2 = np.array([grooveY[2], grooveY[2], grooveY[3], grooveY[3]])

        transformed_points = np.asarray([np.column_stack([x, y])]).astype('float32')        # lots of nesting to make perspectiveTransform work
        transformed_points2 = np.asarray([np.column_stack([x2, y2])]).astype('float32')
        base_points = cv2.perspectiveTransform(transformed_points, inverseTransform).astype('int32')[0]
        base_points2 = cv2.perspectiveTransform(transformed_points2, inverseTransform).astype('int32')[0]

        # Draw reference points
        for coord in base_points:
            cv2.circle(baseImg, coord, radius=7, color=(255, 0, 0), thickness=-1)
        for coord in base_points2:
            cv2.circle(baseImg, coord, radius=7, color=(0, 0, 255), thickness=-1)

        # Calculate slopes
        delta_y = base_points[2][1] - base_points[0][1]
        delta_x_left = (base_points[2][0]-base_points[0][0])
        if np.abs(delta_x_left) < 0.001:
            slope_left = 999
        else:
            slope_left = delta_y/delta_x_left

        delta_x_right = (base_points[3][0]-base_points[1][0])
        if np.abs(delta_x_right) < 0.001:
            slope_right = 999
        else:
            slope_right = delta_y/delta_x_right

        # Calculate intercepts
        int_left = base_points[0][1] - (slope_left * base_points[0][0])
        int_right = base_points[1][1] - (slope_right * base_points[1][0])

        # Get lines
        p1, p2 = self.getExtremePoints(baseImg, slope_left, int_left)
        q1, q2 = self.getExtremePoints(baseImg, slope_right, int_right)

        # Check distance between lines
        width = np.cross(np.array(q1)-np.array(p1),np.array(p2)-np.array(p1))/np.linalg.norm(np.array(p2)-np.array(p1))
        width2 = np.cross(np.array(q2)-np.array(p1),np.array(p2)-np.array(p1))/np.linalg.norm(np.array(p2)-np.array(p1))
        mean_slope = abs(np.arctan(np.mean([slope_left, slope_right])))

        # Calculate distance from rail intercept with camera center to the bottom of the image plane (for distance estimation)
        dp = int(np.mean([slope_left, slope_right])*(baseImg.shape[1]/2 - (p1[0]+q1[0])/2))

        # Check width (top and bottom) against 2nd order polynomial approximation and slope deviation (<5 degrees)
        # top_fit = -41.02*mean_slope**2+103.44*mean_slope-33.37 - width  # 760p
        # bot_fit = -71.43*mean_slope**2+195.92*mean_slope-28.26 - width2 # 760p
        top_fit = -74.19*mean_slope**2+185.16*mean_slope-49.71 - width      # 1520p
        bot_fit = -124.49*mean_slope**2+384.72*mean_slope-72.34 - width2    # 1520p

        line_color = (0, 255, 0)    # greenq2
        if np.abs(top_fit) > 15:
            line_color = (0, 0, 255) # Red
            self.putText(baseImg, '!', (10,80), line_color)
        if np.abs(bot_fit) > 40:
            line_color = (0, 0, 255) # Red
            self.putText(baseImg, '!', (10,120), line_color)

        # Draw lines on image
        cv2.line(baseImg, p1, p2, line_color, 5)
        cv2.line(baseImg, q1, q2, line_color, 5)

        # Write errors for each pair of points on image
        eprint = 'Errors: ' + str(round(grooveE[0],2)) + ', ' + str(round(grooveE[1],2)) + ', ' + str(round(grooveE[2],2)) + ', ' + str(round(grooveE[3],2))
        self.putText(baseImg, eprint, (20,40), (0,0,0))

        # Write groove width on image
        wprint = 'Width top: ' + str(round(width,2)) + ' (' + str(round(top_fit,2)) + ')'
        self.putText(baseImg, wprint, (20,80), (0,0,0))
        wprint = 'Width bot: ' + str(round(width2,2)) + ' (' + str(round(bot_fit,2)) + ')'
        self.putText(baseImg, wprint, (20,120), (0,0,0))

        # Write angle on image
        aprint = 'Angle: ' + str(round(90-mean_slope*180/np.pi, 1))
        self.putText(baseImg, aprint, (20,160), (0,0,0))

        # Draw rail intercept with camera center
        cv2.line(baseImg, (int(baseImg.shape[1]/2-10),dp), (int(baseImg.shape[1]/2+10),dp), (255,0,0), 3, cv2.LINE_AA)
        cv2.line(baseImg, (int(baseImg.shape[1]/2),dp-10), (int(baseImg.shape[1]/2),dp+10), (255,0,0), 3, cv2.LINE_AA)

        return baseImg, dp, width2, mean_slope

    def getAngle(self, T):
        try:
            # Calculates angle change (radians) given perspective transformation T

            # Generate reference image for angle calculation (vertical line)
            dims = (760, 1014)
            ref = np.zeros(dims, dtype=np.uint8)
            ref[:,round(dims[1]/2)] = 255

            #  Apply transformation
            warped = cv2.warpPerspective(ref, T, ref.shape[:2][::-1], flags=cv2.INTER_LINEAR)

            # Perform Hough Transformation to detect lines
            # gray = cv2.cvtColor(cv2.add(ref, warped), cv2.COLOR_BGR2GRAY)
            gray = cv2.add(ref, warped)
            lines = cv2.HoughLines(gray, 1, np.pi / 180, 150, None, 0, 0)

            # Find angle
            angles = []
            for i in range(0, len(lines)):
                angles.append(lines[i][0][1])

            # Compute difference between the two lines
            angle_difference = np.max(angles) - np.min(angles)

            return angle_difference
        except:
            return None

    def getDistance(self, img, angle, dp):
        try:
            alpha = 40*np.pi/180  # Vertical Field of View (radians)
            theta = 61*np.pi/180  # Camera angle wrt vertical plane (radians)
            h = 75                # Camera height wrt ground (cm)
            hi = img.shape[0]     # Image height (pixels)

            # Distance from camera to rail intercept (projected onto the ground plane)
            d = h*np.tan(theta + np.arctan( (hi/2-dp)/(hi/(2*np.tan(alpha/2))) ))
            # Reference: https://www.researchgate.net/publication/274074437_Robust_Vehicle_Detection_and_Distance_Estimation_Under_Challenging_Lighting_Conditions/link/5515e6ba0cf2b5d6a0ec5d5c/download

            # Distance from camera to rail projected onto the vehicle frontal plane
            distance = d*np.sin(angle)

            # # Write angle width on image
            # aprint = 'Dist: ' + str(round(x, 1))
            # self.putText(img, aprint, (20,200), (0,0,0))

            return distance
        except:
            return None