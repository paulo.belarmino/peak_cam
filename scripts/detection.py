#!/usr/bin/env python3

import rospy
# ROS Image message
from sensor_msgs.msg import CompressedImage
# ROS Image message -> OpenCV2 image converter
from cv_bridge import CvBridge, CvBridgeError
# OpenCV2 for saving an image
import cv2
import numpy as np
from trackDetectorUtils import TrackDetection

class GroovesDetector:

    def __init__(self):
        
        # Instantiate CvBridge
        self.bridge = CvBridge()
        self.trackDetection = TrackDetection()
        #rear_peak_topic = rospy.Subscriber("/rear_peak/image_raw/compressed", CompressedImage, self.rear_camera_callback)
        front_peak_topic = rospy.Subscriber("/front_peak/image_raw/compressed", CompressedImage, self.front_camera_callback)
        self.rear_image = np.array([])
        self.front_image = np.array([])

    #Callback function that receives images from the camera
    def front_camera_callback(self,msg):

        try:
            # Convert your ROS Image message to OpenCV2
            cv_image = self.bridge.compressed_imgmsg_to_cv2(msg, desired_encoding='bgr8')
            cv_img_rgb = cv2.cvtColor(cv_image, cv2.COLOR_BGR2RGB)
            img_dims = cv_image.shape
            
            p = True
            # Processes track region
            cropped = cv_img_rgb[
                0 : int(img_dims[0] * 0.6), 0 : img_dims[1], :
            ]  # y0, y1, x0, x1

            # Binary Mask
            prep_tracks = self.trackDetection.preprocessing1520p(cropped, plot=False)
            #Output Hough Lines
            lines_tracks, copy = self.trackDetection.hough1520p(
                cv_image, prep_tracks, region=0, plot=p
            )
            # resized_image = cv2.resize(copy, (600, 600))
            # cv2.imshow("lines_tracks", resized_image)
            # cv2.waitKey(5)
            bev, srcPts, t, t_inv = self.trackDetection.BEV(cv_img_rgb, lines_tracks, plot=p)



            # self.front_image = cv2.resize(cv_img_rgb, (480,264))
            # Resize image and display,  this section is only for diagnosing
            # resized_image = cv2.resize(preview, (300, 300))
            # cv2.imshow("front_camera", resized_image)
            # cv2.waitKey(5)

        except CvBridgeError as e:
            print(e)

    def rear_camera_callback(self,msg):

        try:
            # Convert your ROS Image message to OpenCV2
            cv_image = self.bridge.compressed_imgmsg_to_cv2(msg, desired_encoding='bgr8')
            cv_img_rgb = cv2.cvtColor(cv_image, cv2.COLOR_BGR2RGB)
            # Resize image and display,  this section is only for diagnosing
            #self.rear_image = cv2.resize(cv_img_rgb, (480,264))

            
        except CvBridgeError as e:
            print(e)


if __name__ == "__main__":   

    # Initialize Node   
    rospy.init_node('visual_grooves_detection')
    detector = GroovesDetector()
    # while not rospy.is_shutdown():

    #     if detector.front_image.shape[0] == 264 and detector.rear_image.shape[0] == 264:

    #         combined_image = np.hstack((detector.front_image, detector.rear_image))
    #         cv2.imshow("Front", combined_image)
    #         cv2.waitKey(1)  # Wait for a short period to update the display

    rospy.spin()