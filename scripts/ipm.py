#!/usr/bin/env python3

import rospy
import cv2
import numpy as np
from sensor_msgs.msg import Image, CameraInfo
from cv_bridge import CvBridge

class IPMNode:
    def __init__(self):
        rospy.init_node('ipm_node')

        # Parameters
        self.camera_matrix = np.array([[2606.33043, 0.0, 1085.25385],
                                       [0.0, 2593.59209, 847.99815],
                                       [0.0, 0.0, 1.0]])
        self.distortion_coeffs = np.array([-0.039672, 0.039617, 0.005147, 0.007301, 0.0])

        # Define the source points (corners of the input image)
        self.src_points = np.array([[0, 0],                    # top-left
                                    [2028, 0],                 # top-right
                                    [0, 1520],                 # bottom-left
                                    [2028, 1520]],             # bottom-right
                                    dtype=np.float32)

        # Define the destination points (desired corners of the output image)
        self.dst_points = np.array([[0, 0],                    # top-left
                                    [2028, 0],                 # top-right
                                    [0, 1520],                 # bottom-left
                                    [2028, 1520]],             # bottom-right
                                    dtype=np.float32)

        # Get the IPM transformation matrix
        self.M = cv2.getPerspectiveTransform(self.src_points, self.dst_points)

        # ROS infrastructure
        self.bridge = CvBridge()
        self.image_sub = rospy.Subscriber('/front_peak/image_raw', Image, self.image_callback)
        self.image_pub = rospy.Publisher('/front_peak/ipm_image', Image, queue_size=1)
        rospy.spin()

    def image_callback(self, msg):
        try:
            # Convert the ROS Image message to a CV2 Image
            cv_image = self.bridge.imgmsg_to_cv2(msg, "bgr8")

            # Undistort the image
            undistorted_img = cv2.undistort(cv_image, self.camera_matrix, self.distortion_coeffs)

            # Apply IPM
            ipm_image = cv2.warpPerspective(undistorted_img, self.M, (600, 800))

            # Convert the CV2 Image to ROS Image message and publish
            ipm_msg = self.bridge.cv2_to_imgmsg(ipm_image, "bgr8")
            self.image_pub.publish(ipm_msg)
        except Exception as e:
            rospy.logerr(f"Error in IPM node: {e}")

if __name__ == '__main__':
    try:
        IPMNode()
    except rospy.ROSInterruptException:
        pass
