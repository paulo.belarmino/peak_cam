/*!
 * \file    autofeatures.cpp
 * \author  IDS Imaging Development Systems GmbH
 * \date    2023-05-15
 *
 * \version 1.4.0
 *
 * Copyright (C) 2021 - 2024, IDS Imaging Development Systems GmbH.
 *
 * The information in this document is subject to change without notice
 * and should not be construed as a commitment by IDS Imaging Development Systems GmbH.
 * IDS Imaging Development Systems GmbH does not assume any responsibility for any errors
 * that may appear in this document.
 *
 * This document, or source code, is provided solely as an example of how to utilize
 * IDS Imaging Development Systems GmbH software libraries in a sample application.
 * IDS Imaging Development Systems GmbH does not assume any responsibility
 * for the use or reliability of any portion of this document.
 *
 * General permission to copy or modify is hereby granted.
 */

#include "autofeatures.hpp"
#include <iostream>

AutoFeatures::AutoFeatures(std::shared_ptr<peak::core::NodeMap> nodemap)
    : m_nodemapRemoteDevice(std::move(nodemap))
{
    CreateAutoManager();
    CreateAutoControllers();
}

AutoFeatures::~AutoFeatures()
{
    m_autoFeaturesManager->DestroyAllController();
}

void AutoFeatures::CreateAutoManager()
{
    m_autoFeaturesManager = std::make_unique<peak::afl::Manager>(m_nodemapRemoteDevice);
    m_autoFeaturesManager->SetGainIPL(m_gainControllerIPL);
}

void AutoFeatures::CreateAutoControllers()
{
    m_autoBrightnessController = m_autoFeaturesManager->CreateController(PEAK_AFL_CONTROLLER_TYPE_BRIGHTNESS);

    m_autoBrightnessController->RegisterComponentCallback(PEAK_AFL_CONTROLLER_BRIGHTNESS_COMPONENT_GAIN, [&] {
        if (m_gainFinishedCallback)
        {
            m_gainFinishedCallback();
        }
    });

    m_autoBrightnessController->RegisterComponentCallback(PEAK_AFL_CONTROLLER_BRIGHTNESS_COMPONENT_EXPOSURE, [&] {
        if (m_exposureFinishedCallback)
        {
            m_exposureFinishedCallback();
        }
    });

    m_autoFeaturesManager->AddController(m_autoBrightnessController);

    m_autoWhiteBalanceController = m_autoFeaturesManager->CreateController(PEAK_AFL_CONTROLLER_TYPE_WHITE_BALANCE);
    m_autoWhiteBalanceController->RegisterFinishedCallback([&] {
        if (m_whiteBalanceFinishedCallback)
        {
            m_whiteBalanceFinishedCallback();
        }
    });

    m_autoFeaturesManager->AddController(m_autoWhiteBalanceController);
}

void AutoFeatures::ProcessImage(const peak::ipl::Image* image)
{
    if (m_autoFeaturesManager->Status())
    {
        return;
    }

    try
    {
        m_autoFeaturesManager->Process(*image);
    }
    catch (const std::exception& e)
    {
        std::cout << "Processing image failed: " << e.what();
    }
}


void AutoFeatures::SetExposureMode(peak_afl_controller_automode mode)
{
    m_autoBrightnessController->BrightnessComponentSetMode(PEAK_AFL_CONTROLLER_BRIGHTNESS_COMPONENT_EXPOSURE, mode);
}

void AutoFeatures::SetGainMode(peak_afl_controller_automode mode)
{
    m_autoBrightnessController->BrightnessComponentSetMode(PEAK_AFL_CONTROLLER_BRIGHTNESS_COMPONENT_GAIN, mode);
}

void AutoFeatures::SetWhiteBalanceMode(peak_afl_controller_automode mode)
{
    m_autoWhiteBalanceController->SetMode(mode);
}

void AutoFeatures::SetSkipFrames(int skipFrames)
{
    for (const auto& controller : m_autoFeaturesManager->ControllerList())
    {
        controller->SetSkipFrames(skipFrames);
    }
}

void AutoFeatures::SetGainLimit(const std::pair<double, double>& limit)
{
    m_autoBrightnessController->SetGainLimit({ limit.first, limit.second });
}

std::pair<double, double> AutoFeatures::GainLimit() const
{
    auto limit = m_autoBrightnessController->GetGainLimit();
    return {limit.min, limit.max};
}

void AutoFeatures::SetAutoTarget(const int target)
{
    m_autoBrightnessController->SetAutoTarget(target);
}

int AutoFeatures::GetAutoTarget() const
{
    auto target = m_autoBrightnessController->GetAutoTarget();
    return target;
}

void AutoFeatures::SetAutoTolerance(const int tolerance)
{
    m_autoBrightnessController->SetAutoTolerance(tolerance);
}

int AutoFeatures::GetAutoTolerance() const
{
    auto tolerance = m_autoBrightnessController->GetAutoTolerance();
    return tolerance;
}

void AutoFeatures::SetAutoPercentile(const double percentile)
{
    m_autoBrightnessController->SetAutoPercentile(percentile);
}

double AutoFeatures::GetAutoPercentile() const
{
    auto percentile = m_autoBrightnessController->GetAutoPercentile();
    return percentile;
}

void AutoFeatures::Reset()
{
    for (const auto& controller : m_autoFeaturesManager->ControllerList())
    {
        if (controller->IsBrightnessComponentModeSupported())
        {
            controller->BrightnessComponentSetMode(
                PEAK_AFL_CONTROLLER_BRIGHTNESS_COMPONENT_EXPOSURE, PEAK_AFL_CONTROLLER_AUTOMODE_OFF);
            controller->BrightnessComponentSetMode(
                PEAK_AFL_CONTROLLER_BRIGHTNESS_COMPONENT_GAIN, PEAK_AFL_CONTROLLER_AUTOMODE_OFF);
        }
        else
        {
            controller->SetMode(PEAK_AFL_CONTROLLER_AUTOMODE_OFF);
        }
    }
}

void AutoFeatures::RegisterExposureCallback(std::function<void(void)> callback)
{
    m_exposureFinishedCallback = std::move(callback);
}

void AutoFeatures::RegisterGainCallback(std::function<void(void)> callback)
{
    m_gainFinishedCallback = std::move(callback);
}

void AutoFeatures::RegisterWhiteBalanceCallback(std::function<void(void)> callback)
{
    m_whiteBalanceFinishedCallback = std::move(callback);
}
void AutoFeatures::PrintAutoPrams()
{
    auto getAutoTarget = GetAutoTarget();
    auto getAutoTargetRange = m_autoBrightnessController->GetAutoTargetRange();
    std::cout << "getAutoTarget: " << getAutoTarget << ", getAutoTargetRange min: " 
              << getAutoTargetRange.min  << ", getAutoTargetRange max: " << getAutoTargetRange.max  << std::endl;

    auto getAutoTolerance = GetAutoTolerance();
    auto getAutoToleranceRange = m_autoBrightnessController->GetAutoToleranceRange();
    std::cout << "getAutoTolerance: " << getAutoTolerance << ", getAutoToleranceRange min: " 
              << getAutoToleranceRange.min  << ", getAutoToleranceRange max: " << getAutoToleranceRange.max  << std::endl;

    auto getAutoPercentile = GetAutoPercentile();
    auto getAutoPercentileRange = m_autoBrightnessController->GetAutoPercentileRange();
    std::cout << "getAutoPercentile: " << getAutoPercentile << ", getAutoPercentileRange min: " 
              << getAutoPercentileRange.min  << ", getAutoPercentileRange max: " << getAutoPercentileRange.max  << std::endl;

    auto algorithmSupported = m_autoBrightnessController->IsAlgorithmSupported();
    auto getAlgorithm = m_autoBrightnessController->GetAlgorithm();
    std::cout << "algorithmSupported: " << algorithmSupported 
              << ", getAlgorithm: "  << getAlgorithm << std::endl;

    // auto getAlgorithmList = m_autoBrightnessController->GetAlgorithmList();
    // std::cout << "getAlgorithmList size: " << sizeof(getAlgorithmList) << std::endl;
    // std::cout << "getAlgorithmList" << std::endl;      
    // for (int i = 0; i < sizeof(getAlgorithmList); ++i) 
    // {
    //     std::cout << getAlgorithmList[i] << std::endl;
    // }

    auto sharpnessAlgorithmSupported = m_autoBrightnessController->IsSharpnessAlgorithmSupported();
    std::cout << "sharpnessAlgorithmSupported: " << sharpnessAlgorithmSupported  << std::endl;

    auto isLimitSupported = m_autoBrightnessController->IsLimitSupported();
    std::cout << "isLimitSupported: " << isLimitSupported << std::endl;


    auto getGainLimit = m_autoBrightnessController->GetGainLimit();
    auto getGainLimitRange = m_autoBrightnessController->GetGainLimitRange();
    std::cout << "getGainLimit min: " << getGainLimit.min 
              << ", getGainLimit max: " << getGainLimit.max
              << ", getGainLimitRange min: " << getGainLimitRange.min  
              << ", getGainLimitRange max: " << getGainLimitRange.max  << std::endl;

}